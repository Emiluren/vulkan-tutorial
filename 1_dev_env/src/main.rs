fn main() {
    let entry = ash::Entry::linked();
    let extensions = entry.enumerate_instance_extension_properties().unwrap();
    println!("{} extensions supported", extensions.len());
}
