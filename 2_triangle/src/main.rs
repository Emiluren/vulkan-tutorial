use ash::{vk, Entry, extensions::khr::Swapchain, prelude::VkResult};
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    platform::run_return::EventLoopExtRunReturn,
    window::WindowBuilder,
};

use std::{error::Error, os::raw::c_char};
use std::ffi::CStr;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT: u32 = 600;

unsafe fn create_instance(
    entry: &ash::Entry,
    window: &winit::window::Window,
    requested_layers: &[*const i8]
) -> Result<ash::Instance, Box<dyn Error>> {
    let app_info = vk::ApplicationInfo::builder()
        .application_name(CStr::from_bytes_with_nul_unchecked(b"Hello Triangle\0"))
        .api_version(vk::make_api_version(0, 1, 0, 0));

    let extension_names: Vec<_> = ash_window::enumerate_required_extensions(window)?.iter()
        .map(|name| name.as_ptr()).collect();
    let create_info = vk::InstanceCreateInfo::builder()
        .application_info(&app_info)
        .enabled_layer_names(requested_layers)
        .enabled_extension_names(&extension_names);

    Ok(entry.create_instance(&create_info, None)?)
}

unsafe fn pick_device(
    instance: &ash::Instance,
    surface: vk::SurfaceKHR,
    surface_loader: &ash::extensions::khr::Surface,
) -> (vk::PhysicalDevice, u32) {
    instance.enumerate_physical_devices()
        .expect("Could not enumerate devices")
        .iter()
        .find_map(|pdevice| {
            instance.get_physical_device_queue_family_properties(*pdevice)
                .iter()
                .enumerate()
                .find_map(|(index, info)| {
                    let supports_graphics_and_surface =
                        info.queue_flags.contains(vk::QueueFlags::GRAPHICS) &&
                        surface_loader
                            .get_physical_device_surface_support(*pdevice, index as u32, surface)
                            .unwrap();
                    if supports_graphics_and_surface {
                        Some((*pdevice, index as u32))
                    } else {
                        None
                    }
                })
        })
        .expect("Couldn't find suitable device.")
}

unsafe fn create_logical_device(
    instance: &ash::Instance,
    physical_device: vk::PhysicalDevice,
    queue_family_index: u32,
) -> VkResult<ash::Device> {
    let queue_info = vk::DeviceQueueCreateInfo::builder()
        .queue_family_index(queue_family_index)
        .queue_priorities(&[1.0])
        .build();

    let device_extension_names = [Swapchain::name().as_ptr()];
    let device_create_info = vk::DeviceCreateInfo::builder()
        .queue_create_infos(std::slice::from_ref(&queue_info))
        .enabled_extension_names(&device_extension_names);

    instance.create_device(physical_device, &device_create_info, None)
}

fn surface_res(surface_capabilities: &vk::SurfaceCapabilitiesKHR) -> vk::Extent2D {
    let extent = surface_capabilities.current_extent;
    if extent.width == std::u32::MAX {
        vk::Extent2D {
            width: WINDOW_WIDTH,
            height: WINDOW_HEIGHT,
        }
    } else {
        extent
    }
}

unsafe fn create_swapchain(
    instance: &ash::Instance,
    device: &ash::Device,
    surface: vk::SurfaceKHR,
    surface_format: &vk::SurfaceFormatKHR,
    surface_capabilities: &vk::SurfaceCapabilitiesKHR,
) -> VkResult<(vk::SwapchainKHR, Swapchain)> {
    let mut desired_image_count = surface_capabilities.min_image_count + 1;
    if surface_capabilities.max_image_count > 0  {
        desired_image_count = desired_image_count
            .min(surface_capabilities.max_image_count);
    }

    let swapchain_loader = Swapchain::new(&instance, &device);

    let swapchain_create_info = vk::SwapchainCreateInfoKHR::builder()
        .surface(surface)
        .min_image_count(desired_image_count)
        .image_color_space(surface_format.color_space)
        .image_format(surface_format.format)
        .image_extent(surface_res(surface_capabilities))
        .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
        .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
        .pre_transform(surface_capabilities.current_transform)
        .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
        .present_mode(vk::PresentModeKHR::FIFO)
        .clipped(true)
        .image_array_layers(1);

    swapchain_loader.create_swapchain(&swapchain_create_info, None)
        .map(|sc| (sc, swapchain_loader))
}

unsafe fn create_image_views(
    device: &ash::Device,
    surface_format: &vk::SurfaceFormatKHR,
    swapchain_images: &[vk::Image],
) -> Vec<vk::ImageView> {
    swapchain_images.iter()
        .map(|&image| {
            let create_view_info = vk::ImageViewCreateInfo::builder()
                .view_type(vk::ImageViewType::TYPE_2D)
                .format(surface_format.format)
                .components(vk::ComponentMapping::default())
                .subresource_range(vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::COLOR,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                })
                .image(image);
            device.create_image_view(&create_view_info, None).unwrap()
        })
        .collect()
}

unsafe fn create_render_pass(
    device: &ash::Device,
    surface_format: vk::Format
) -> VkResult<vk::RenderPass> {
    let renderpass_attachments = [vk::AttachmentDescription {
        format: surface_format,
        samples: vk::SampleCountFlags::TYPE_1,
        load_op: vk::AttachmentLoadOp::CLEAR,
        store_op: vk::AttachmentStoreOp::STORE,
        final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
        ..Default::default()
    }];
    let color_attachment_refs = [vk::AttachmentReference {
        attachment: 0,
        layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
    }];

    let subpass = vk::SubpassDescription::builder()
        .color_attachments(&color_attachment_refs)
        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS);

    let dependencies = [vk::SubpassDependency{
        src_subpass: vk::SUBPASS_EXTERNAL,
        src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
        dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
        dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
        ..Default::default()
    }];

    let renderpass_create_info = vk::RenderPassCreateInfo::builder()
        .attachments(&renderpass_attachments)
        .subpasses(std::slice::from_ref(&subpass))
        .dependencies(&dependencies);

    device.create_render_pass(&renderpass_create_info, None)
}

unsafe fn load_shader(filename: &str, device: &ash::Device) -> Result<vk::ShaderModule, Box<dyn Error>> {
    let mut file = std::fs::File::open(filename)?;
    let code = ash::util::read_spv(&mut file)?;
    let info = vk::ShaderModuleCreateInfo::builder().code(&code);
    Ok(device.create_shader_module(&info, None)?)
}

unsafe fn create_graphics_pipeline(
    device: &ash::Device,
    surf_res: vk::Extent2D,
    render_pass: vk::RenderPass,
) -> Result<(vk::PipelineLayout, vk::Pipeline), Box<dyn Error>> {
    let shader_modules = ["vert.spv", "frag.spv"].into_iter().map(
        |filename| load_shader(filename, &device)
    ).collect::<Result<Vec<_>, _>>()?;

    let main_str = CStr::from_bytes_with_nul_unchecked(b"main\0");
    let shader_stage_infos = [
        vk::PipelineShaderStageCreateInfo {
            module: shader_modules[0],
            p_name: main_str.as_ptr(),
            stage: vk::ShaderStageFlags::VERTEX,
            ..Default::default()
        },
        vk::PipelineShaderStageCreateInfo {
            module: shader_modules[1],
            p_name: main_str.as_ptr(),
            stage: vk::ShaderStageFlags::FRAGMENT,
            ..Default::default()
        },
    ];

    let vertex_input_state_info = vk::PipelineVertexInputStateCreateInfo::builder();
    let vertex_input_assembly_info = vk::PipelineInputAssemblyStateCreateInfo {
        topology: vk::PrimitiveTopology::TRIANGLE_LIST,
        ..Default::default()
    };

    let viewports = [vk::Viewport {
        x: 0.0,
        y: 0.0,
        width: surf_res.width as f32,
        height: surf_res.height as f32,
        min_depth: 0.0,
        max_depth: 1.0,
    }];
    let scissors = [vk::Rect2D {
        offset: vk::Offset2D { x: 0, y: 0 },
        extent: surf_res,
    }];
    let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
        .scissors(&scissors)
        .viewports(&viewports);

    let rasterization_info = vk::PipelineRasterizationStateCreateInfo {
        front_face: vk::FrontFace::CLOCKWISE,
        line_width: 1.0,
        polygon_mode: vk::PolygonMode::FILL,
        ..Default::default()
    };
    let multisample_state_info = vk::PipelineMultisampleStateCreateInfo {
        rasterization_samples: vk::SampleCountFlags::TYPE_1,
        ..Default::default()
    };
    let color_blend_attachment_states = [vk::PipelineColorBlendAttachmentState {
        blend_enable: 0,
        color_write_mask:
            vk::ColorComponentFlags::R |
            vk::ColorComponentFlags::G |
            vk::ColorComponentFlags::B |
            vk::ColorComponentFlags::A,
        ..Default::default()
    }];
    let color_blend_state = vk::PipelineColorBlendStateCreateInfo::builder()
        .attachments(&color_blend_attachment_states);

    let layout_create_info = vk::PipelineLayoutCreateInfo::default();
    let pipeline_layout = device.create_pipeline_layout(&layout_create_info, None)?;

    let graphic_pipeline_info = vk::GraphicsPipelineCreateInfo::builder()
        .stages(&shader_stage_infos)
        .vertex_input_state(&vertex_input_state_info)
        .input_assembly_state(&vertex_input_assembly_info)
        .viewport_state(&viewport_state_info)
        .rasterization_state(&rasterization_info)
        .multisample_state(&multisample_state_info)
        .color_blend_state(&color_blend_state)
        .layout(pipeline_layout)
        .render_pass(render_pass)
        .build();

    let pipeline = device.create_graphics_pipelines(
        vk::PipelineCache::null(),
        &[graphic_pipeline_info],
        None
    ).map_err(|(_, err)| err)?[0];

    for module in shader_modules {
        device.destroy_shader_module(module, None);
    }

    Ok((pipeline_layout, pipeline))
}

unsafe fn create_framebuffers(
    device: &ash::Device,
    image_views: &[vk::ImageView],
    surface_resolution: vk::Extent2D,
    render_pass: vk::RenderPass,
) -> VkResult<Vec<vk::Framebuffer>> {
    image_views.iter()
        .map(|&image_view| {
            let attachments = [image_view];
            let create_info = vk::FramebufferCreateInfo::builder()
                .render_pass(render_pass)
                .attachments(&attachments)
                .width(surface_resolution.width)
                .height(surface_resolution.height)
                .layers(1);

            device.create_framebuffer(&create_info, None)
        }).collect()
}

unsafe fn create_command_buffers(
    device: &ash::Device,
    framebuffers: &[vk::Framebuffer],
    queue_family_index: u32,
    render_pass: vk::RenderPass,
    surf_res: vk::Extent2D,
    pipeline: vk::Pipeline,
) -> VkResult<(vk::CommandPool, Vec<vk::CommandBuffer>)> {
    let pool_create_info = vk::CommandPoolCreateInfo::builder()
        .queue_family_index(queue_family_index);
    let pool = device.create_command_pool(&pool_create_info, None)?;

    let allocate_info = vk::CommandBufferAllocateInfo::builder()
        .command_buffer_count(framebuffers.len() as u32)
        .command_pool(pool)
        .level(vk::CommandBufferLevel::PRIMARY);
    let command_buffers = device.allocate_command_buffers(&allocate_info)?;

    let clear_value = vk::ClearValue {
        color: vk::ClearColorValue {
            float32: [0.0, 0.0, 0.0, 0.0],
        }
    };

    for (i, &cmd_buffer) in command_buffers.iter().enumerate() {
        let begin_info = vk::CommandBufferBeginInfo::builder();
        device.begin_command_buffer(cmd_buffer, &begin_info)?;

        let render_pass_info = vk::RenderPassBeginInfo::builder()
            .render_pass(render_pass)
            .framebuffer(framebuffers[i])
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: surf_res,
            })
            .clear_values(std::slice::from_ref(&clear_value));

        device.cmd_begin_render_pass(cmd_buffer, &render_pass_info, vk::SubpassContents::INLINE);
        device.cmd_bind_pipeline(cmd_buffer, vk::PipelineBindPoint::GRAPHICS, pipeline);
        device.cmd_draw(cmd_buffer, 3, 1, 0, 0);
        device.cmd_end_render_pass(cmd_buffer);

        device.end_command_buffer(cmd_buffer)?;
    }

    Ok((pool, command_buffers))
}

const MAX_FRAMES_IN_FLIGHT: usize = 2;

fn main() -> Result<(), Box<dyn Error>> {
    let mut event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(LogicalSize::new(WINDOW_WIDTH, WINDOW_HEIGHT))
        .build(&event_loop)?;

    let entry = Entry::linked();

    let requested_layers = [b"VK_LAYER_KHRONOS_validation\0".as_ptr() as *const c_char];

    unsafe {
        let extensions = entry.enumerate_instance_extension_properties()?;
        println!("Available extensions:");

        for ext in extensions {
            let ext_name = CStr::from_ptr(ext.extension_name.as_ptr());
            println!("\t{}", ext_name.to_str()?);
        }
        println!();

        let layers = entry.enumerate_instance_layer_properties()?;
        println!("Available layers:");

        for lay in layers {
            let lay_name = CStr::from_ptr(lay.layer_name.as_ptr());
            println!("\t{}", lay_name.to_str()?);
        }

        let instance = create_instance(&entry, &window, &requested_layers)?;
        let surface = ash_window::create_surface(&entry, &instance, &window, None)?;
        let surface_loader = ash::extensions::khr::Surface::new(&entry, &instance);
        let (pdevice, queue_family_index) = pick_device(&instance, surface, &surface_loader);
        let device = create_logical_device(&instance, pdevice, queue_family_index)?;
        let graphics_queue = device.get_device_queue(queue_family_index, 0);

        let surface_format = surface_loader.get_physical_device_surface_formats(pdevice, surface)?[0];
        let surface_capabilities = surface_loader.get_physical_device_surface_capabilities(pdevice, surface)?;
        let (swapchain, swapchain_loader) = create_swapchain(
            &instance, &device, surface, &surface_format, &surface_capabilities
        )?;
        let surf_res = surface_res(&surface_capabilities);

        let swapchain_images = swapchain_loader.get_swapchain_images(swapchain)?;
        let swapchain_image_views = create_image_views(&device, &surface_format, &swapchain_images);

        let render_pass = create_render_pass(&device, surface_format.format)?;
        let (pipeline_layout, pipeline) = create_graphics_pipeline(&device, surf_res, render_pass)?;

        let framebuffers = create_framebuffers(
            &device, &swapchain_image_views, surf_res, render_pass
        )?;

        let (command_pool, command_buffers) = create_command_buffers(
            &device, &framebuffers, queue_family_index, render_pass, surf_res, pipeline
        )?;

        let semaphore_info = vk::SemaphoreCreateInfo::default();
        let fence_info = vk::FenceCreateInfo::builder().flags(vk::FenceCreateFlags::SIGNALED);
        let mut image_available_semaphores = Vec::new();
        let mut render_finished_semaphores = Vec::new();
        let mut in_flight_fences = Vec::new();
        let mut images_in_flight = vec![vk::Fence::null(); swapchain_images.len()];
        for _ in 0..MAX_FRAMES_IN_FLIGHT {
            image_available_semaphores.push(device.create_semaphore(&semaphore_info, None)?);
            render_finished_semaphores.push(device.create_semaphore(&semaphore_info, None)?);
            in_flight_fences.push(device.create_fence(&fence_info, None)?);
        }

        let mut current_frame = 0;
        event_loop.run_return(|event, _, control_flow| {
            *control_flow = ControlFlow::Wait;
            match event {
                Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => {
                    *control_flow = ControlFlow::Exit;
                }
                Event::MainEventsCleared => {
                    window.request_redraw();
                }
                Event::RedrawRequested(_) => {
                    device.wait_for_fences(&[in_flight_fences[current_frame]], true, u64::MAX).unwrap();

                    let (image_index, _) = swapchain_loader.acquire_next_image(
                        swapchain, u64::MAX, image_available_semaphores[current_frame], vk::Fence::null()
                    ).unwrap();

                    if images_in_flight[image_index as usize] != vk::Fence::null() {
                        let image_fence = [images_in_flight[image_index as usize]];
                        device.wait_for_fences(&image_fence, true, u64::MAX).unwrap();
                    }
                    images_in_flight[image_index as usize] = in_flight_fences[current_frame];

                    let submit_info = vk::SubmitInfo::builder()
                        .wait_semaphores(&[image_available_semaphores[current_frame]])
                        .wait_dst_stage_mask(&[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                        .command_buffers(&[command_buffers[image_index as usize]])
                        .signal_semaphores(&[render_finished_semaphores[current_frame]])
                        .build();

                    device.reset_fences(&[in_flight_fences[current_frame]]).unwrap();

                    device.queue_submit(graphics_queue, &[submit_info], in_flight_fences[current_frame])
                        .expect("Failed to submit draw command buffer");

                    let present_info = vk::PresentInfoKHR::builder()
                        .wait_semaphores(std::slice::from_ref(&render_finished_semaphores[current_frame]))
                        .swapchains(std::slice::from_ref(&swapchain))
                        .image_indices(std::slice::from_ref(&image_index));

                    swapchain_loader.queue_present(graphics_queue, &present_info).unwrap();

                    current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
                }
                _ => {}
            }
        });

        device.device_wait_idle().unwrap();

        device.destroy_pipeline(pipeline, None);
        device.destroy_pipeline_layout(pipeline_layout, None);
        for framebuffer in framebuffers {
            device.destroy_framebuffer(framebuffer, None);
        }
        device.destroy_render_pass(render_pass, None);

        for i in 0..MAX_FRAMES_IN_FLIGHT {
            device.destroy_semaphore(image_available_semaphores[i], None);
            device.destroy_semaphore(render_finished_semaphores[i], None);
            device.destroy_fence(in_flight_fences[i], None);
        }
        for image_view in swapchain_image_views {
            device.destroy_image_view(image_view, None);
        }
        device.destroy_command_pool(command_pool, None);
        swapchain_loader.destroy_swapchain(swapchain, None);
        device.destroy_device(None);
        surface_loader.destroy_surface(surface, None);
        instance.destroy_instance(None);

        Ok(())
    }
}
